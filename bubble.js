window.onload = function () {

    /*variables*/
    var inputArrayLength = 0;
    var input = document.getElementById('inputArrayLength');
    var buttonGenEl = document.querySelector('#buttonGen');
    var buttonSortEl = document.querySelector('#buttonSort');


    /*triggering events*/
    document.onkeydown = function (e) {
        e = e || window.event;
        if (e.keyCode === 13) {
            visualiserObject.init(input.value);
        }
    };

    var visualiserObject = new Vizualizer({
        output: '#visualArea',
        maxItems: 54,
        duration: {
            'durationPhase1': 500,
            'durationPhase1.1': 500,
            'durationPhase1.2': 500,
            'durationPhase1.2.1': 500,
            'durationPhase2': 500
        },
        complete: function () {
            console.log('функция обратного вызова по завершению работы сортировки')
        }

    });
    visualiserObject.init(4);
    visualiserObject.sort();
    visualiserObject.changeDirection();
    visualiserObject.sort();


};
function Vizualizer(object) {

    this.output = object.output;
    this.maxItems = object.maxItems;
    this.duration = object.duration;
    this.dir = -1;
    this.array = [];


    this.init = function (number) {
        if (number > this.maxItems) {
            return false;
        }
        for (var i = 0; i < number; i++) {
            this.array[i] = Math.round(Math.random() * 100);
            var visualItem = document.createElement('div');
            visualItem.className = 'visualArrayItem';
            visualItem.innerHTML = this.array[i];
            document.querySelector(this.output).appendChild(visualItem)
        }
    };
    this.sort = function () {
        for (var j = 0; j < this.array.length; j++) {

            for (var i = 0; i < this.array.length - 1; i++) {

                if (this.dir * this.array[i] > this.dir * this.array[i + 1]) {

                    var k = this.array[i];

                    this.array[i] = this.array[i + 1];

                    this.array[i + 1] = k;
                }
            }
        }
        console.log(this.array)
        return this.array;
    };
    this.changeDirection = function () {
        this.dir *= -1;

    };
}
